import java.util.ArrayList;
import java.util.Arrays;

public class ArrayListDemo {

    public static void main(String[] args) {


        int[][] vals= new int[3][7];

        System.out.println(vals.getClass().getName());

        String[] strList = new String[2];
        strList[0] = "Hello";
        strList[1] = "World";


        System.out.println(Arrays.toString(strList));

        for(int i=0; i< strList.length; i++){
            System.out.print(strList[i] + " ");
        }

        System.out.println();
        for(String str : strList){
            System.out.print(str+ " ");
        }

        //strList[2] = "Mugla";

        System.out.println("Array List");
        ArrayList<String> strList2 = new ArrayList<>();
        //strList2.add(0,"Hello");
        strList2.add("Hello");
        strList2.add("World");
        strList2.add("Mugla");
        strList2.add("Kotekli");


        for(int i=0; i< strList2.size(); i++){
            System.out.print(strList2.get(i) + " ");
        }
        strList2.set(1,"Earth");
        System.out.println();
        for(String str : strList2){
            System.out.print(str+ " ");
        }
        strList2.remove(2);
        System.out.println();
        System.out.println(strList2);

        ArrayList<Double> values = new ArrayList<Double>();

        values.add(5.0);


        ArrayList<String> strList3 = new ArrayList<>();
        strList3.add("a");
        strList3.add("b");

        strList2.addAll(strList3);

        System.out.println(strList2);
    }
}
