package shape;

public class Rectangle extends Shape{

    int width;
    int length;

    public Rectangle(){

        super("WHITE");
        System.out.println(" Rectangle is being inialized");
        width =9;
    }

    public Rectangle(String color){

        super(color);
        System.out.println(" Rectangle is being inialized");
        width =9;
    }

    public Rectangle(String color, int width, int length) {
        super(color);
        this.width = width;
        this.length = length;
    }


    public int area(){
        return width * length;
    }
}
