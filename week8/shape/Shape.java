package shape;

public class Shape {

    private String color;


    public Shape(String color) {
        this.color = color;
        System.out.println("Shape is being initialized with color " + color);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
