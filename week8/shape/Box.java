package shape;

public class Box extends  Rectangle{

    int height;

    public Box(String color){
        super(color);
        System.out.println(" Initializing Box");
    }

    public Box(String color, int width, int length, int height) {
        super(color, width, length);
        this.height = height;
    }

    public int area(){
        return 2 * (super.area() + width * height + height * length);
    }
}
