public class Main {

    public static void main(String[] args){
        Point point1 = new Point();
        point1.xCoord = 5.2;

        point1.move(1,1);

        System.out.println(point1.xCoord);
        System.out.println(point1.yCoord);


        System.out.println("distance of p1 " + point1.distancefromOrigin());

        Point point2 = new Point( 3.2, 2);

        System.out.println(point2.xCoord);
        System.out.println(point2.yCoord);


        System.out.println("distance of p2 " +point2.distancefromOrigin());
        Point[] points = new Point[5];
        points[0] = point1;


        String str1 = new String("Hello");
        System.out.println(str1.length());

        String str2 = "Merhaba";
        System.out.println(str2.length());


        System.out.println(point1.distancefromPoint(point2));
        System.out.println(point2.distancefromPoint(point1));

        System.out.println(point1.distancefromPoint(point1));
    }
}
