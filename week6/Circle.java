public class Circle {

    private int radius;

    private Point center;

    static final double PI = 3.14;

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }

    public Circle() {
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }
}
