public class ParameterPassing {

    static String org;

    public static void main(String[] args){

        int x = 5;
        x = increment(x);
        System.out.println(x);

        String str = "Hello";
        org = str;
        updateString(str);
        System.out.println(str);


        Point p = new Point();
        p.xCoord = 5;
        p.yCoord = 3;


        updatePoint(p);

        System.out.println("p.x = " + p.xCoord);
        System.out.println("p.x = " + p.yCoord);

    }

    public static int increment(int a){
        System.out.println(a);
        a = 10;
        System.out.println(a);
        return a;
    }

    public static void updateString(String str){
        if (org == str){
            System.out.println("same objects before assigned");
        }else{
            System.out.println("different objects before assigned");
        }
        str = "World";

        if (org == str){
            System.out.println("same objects after assigned");
        }else{
            System.out.println("different objects after assigned");
        }
    }


    public static void updatePoint(Point p){
        p = new Point();
        p.xCoord = 0;
        p.yCoord = 0;
    }

}
