public class ScopeDemo {

    static int var3;

    int var1 = 0;

    public ScopeDemo() {
        this(5);
        System.out.println("Default const executeds");
    }

    public ScopeDemo(int var1) {
        this.var1 = var1;
        System.out.println("Constructor with a param execute");
    }

    public void m1(int var1){
        this.var1 = var1;
        int var2 = 5;
    }

    public void m2(){
        var1 = 5;
        //var2 = 4;
    }

    public static void m3(){
        //var1 = 5;
    }
}
