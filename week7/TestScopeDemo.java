public class TestScopeDemo {

    public static void main(String[] args){

        //ScopeDemo.m1();

        ScopeDemo.m3();

        ScopeDemo.var3 = 8;

        ScopeDemo sd = new ScopeDemo();

        sd.m1(6);
        sd.m2();

        ScopeDemo sd2 = new ScopeDemo(7);

        sd2.m1(4);
    }
}
