public class HelloWorld{
	
	public static void main(String[] args){
		System.out.print("Hello World \n");
		
		int a = foo();
		System.out.println(a);
		System.out.println(varExample());
		
	}
	
	
	
	public static int foo(){
		System.out.println("Hello" + " Again");
		return 5;
	}
	
	
	public static int varExample(){
		String name = "Ali";
		boolean isClosed = true;
		double pi= 3;
		
		long number = 2147483648L;
		
		int var1 = 1024;
		int var2 = var1/2;
		
		System.out.println("Var1 contains " + var1);
		System.out.println("pi =  " + pi);
		return var1 + var2;
	}
	
}