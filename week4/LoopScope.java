public class LoopScope {
    public static void main(String[] args){

        int i = 0, z = 8;
        int sum = 0;
        for(; i <5; i++){

            System.out.println("i = " + i);
            sum += i;
        }
        System.out.println("i = " + i);
        System.out.println("sum = " + sum);


        int k = 10;
        int j = 0;
        for( ;j<k;){
            System.out.println("j = " + j);
            System.out.println("k = " + k);
            j++;
            k--;
        }


        System.out.println("j = " + j);


        z = 0;
        while(true){
            System.out.println("z = " + z);
            if (z == 5)
                break;
            z++;
        }



        for(z = 0;z<10;z++) {
            System.out.println("Hello");
            System.out.println("Hello  Again");
        }

        //m++;
    }
    
}
